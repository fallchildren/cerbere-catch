;;; cerbere-catch.el --- Cerbere module for Catch

;; Copyright (C) 2016  Sebastian Fieber <sebastian.fieber@web.de>

;; Author: Sebastian Fieber <sebastian.fieber@web.de>
;; URL: https://github.com/fallchildren/cerbere-catch
;; Version: 0.0.9
;; Keywords: c++, tdd, tests

;; Package-Requires: ((cerbere "0.1.0"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Cebere module for Catch (https://github.com/philsquared/Catch) the
;; "modern, C++-native, header-only, framework for unit-tests, TDD and
;; BDD C++ Automated Test Cases in Headers".
;;
;; To get started put something like
;;
;; (require 'cerbere)
;; (require 'cerbere-catch)
;; (cerbere-catch-setup)
;; (cerbere-global-mode t)
;;
;; into your .emacs/init.el.
;;
;; Also you have to customize some variables of which the most should
;; be dir- or buffer-local.  Following variables must be set:
;;
;; `cerbere-catch-compile-command' which is the command to compile
;; your test binary.  Your Makefile knows best how to do that.
;;
;; `cerbere-catch-program' which is the path to your test binary
;; starting at your project root.
;;
;; Both variables are project dependent and both are called from your
;; project root.
;;
;; `cerbere-catch-testcase-prefix' and `cerbere-catch-testcase-suffix'
;; for customizing the projects naming convention corresponding
;; tests.  And equivalent to those `cerbere-catch-testfile-prefix' and
;; `cerbere-catch-testfile-suffix' for your test files.
;;
;; If you want even more customization you can also provide custom
;; functions for finding your test/tag for the source file/test.  For
;; this you can customize `cerbere-catch-custom-get-tag-function' and
;; `cerbere-catch-custom-get-test-name-function' if the ones provided
;; do not satisfy the needs of the project.
;;
;; Features and Quirks:
;;
;; The functions cerbere provides (`cerbere-current-test'
;; `cerbere-current-file' and `cerbere-current-project') work mostly
;; as expected.  Depending on which file you are
;; `cerbere-current-file' will behave slightly different.
;;
;; If inside a test file `cerbere-current-file' will not launch all
;; tests of the file but call all tests of the tag(s) which the
;; current test corresponds to.  If inside one of your projects normal
;; files the command will launch all tests inside the test file using
;; the --filenames-as-tags (-#) option.
;;
;; I hope this behaviour handles even BDD naming styles for tests
;; 'okay'.

;;; Code:

(require 'cerbere)
(require 'cerbere-common)
(require 'cl-seq)
(require 'subr-x)
(require 'which-func)


;; Custom stuff

(defgroup cerbere-catch nil
  "Cerbere module for Catch C++ Testframework."
  :group 'cerbere)

(defgroup cerbere-catch-local-vars nil
  "Cerbere Catch local variables.

All variables in this group should be set as buffer local
variables for a specific project. Set them via dir-locals or
buffer-locals."

  :group 'cerbere-catch)

(defcustom cerbere-catch-stop-on-failure nil
  "If non nil --abort option will be set."
  :type 'boolean
  :group 'cerbere-catch)

(defcustom cerbere-catch-stop-on-x-failures nil
  "Specify the argument to the --abortx flag.

Can either be nil or an integer.  If nil the option will not be
set."

  :type '(restricted-sexp :match-alternatives
                          (integerp 'nil))
  :group 'cerbere-catch)

(defcustom cerbere-catch-no-throw nil
  "If non nil --nothrow flag will be set."
  :type 'boolean
  :group 'cerbere-catch)

(defcustom cerbere-catch-include-successful nil
  "If non nil --success flag will be set."
  :type 'boolean
  :group 'cerbere-catch)

(defcustom cerbere-catch-break-into-debugger nil
  "EXPERIMENTAL - If non nil --break flag will be set.

At the moment Catch only supports MS Visual Studio and Apples
XCode to break into the debugger.  If you tweak catch.hpp to
throw a SIGINT you can experiment around with this.  At the
moment this variable will start the test binary through gdb in
EMACS which has a few quirks.  See
`cerbere-catch--break-into-debugger-advice' and
`cerbere-catch--get-program' for more information."

  :type 'boolean
  :group 'cerbere-catch)

(defcustom cerbere-catch-show-durations t
  "If nil durations will not be shown."
  :type 'boolean
  :group 'cerbere-catch)

(defcustom cerbere-catch-order "decl"
  "The --order flag value."
  :type '(choice (string :tag "Declaration order" "decl")
                 (string :tag "Lexicographically sorted" "lex")
                 (string :tag "Randomly sorted" "rand"))
  :group 'cerbere-catch)

(defcustom cerbere-catch-rng-seed nil
  "Specify the argument to the --rng-seed option.

If nil --rng-seed is not set."

  :type '(restricted-sexp :match-alternatives
                          (stringp 'nil))
  :group 'cerbere-catch)



;; Per project local custom variables

(defcustom cerbere-catch-compile-command "make test"
  "Defines the command to compile `cerbere-catch-program'."
  :type '(restricted-sexp :match-alternatives
                          (stringp 'nil))
  :group 'cerbere-catch-local-vars)

(defcustom cerbere-catch-program nil
  "Defines the command which is called for tests.

Should be the output of `cerbere-catch-compile-command'."

  :type '(restricted-sexp :match-alternatives
                          (stringp 'nil))
  :group 'cerbere-catch-local-vars)

(defcustom cerbere-catch-testcase-prefix nil
  "The naming prefix for tests in the current project."
  :type '(restricted-sexp :match-alternatives
                          (stringp 'nil))
  :group 'cerbere-catch-local-vars)

(defcustom cerbere-catch-testcase-suffix "Test"
  "The naming suffix for tests in the current project."
  :type '(restricted-sexp :match-alternatives
                          (stringp 'nil))
  :group 'cerbere-catch-local-vars)

(defcustom cerbere-catch-testfile-prefix nil
  "The naming prefix for your test files.

See the documentation of `cerbere-catch--get-current-tag' for
usage."

  :type '(restricted-sexp :match-alternatives
                          (stringp 'nil))
  :group 'cerbere-catch-local-vars)

(defcustom cerbere-catch-testfile-suffix nil
  "The naming suffix for your test files.

See the documentation of `cerbere-catch--get-current-tag' for
usage."

  :type '(restricted-sexp :match-alternatives
                          (stringp 'nil))
  :group 'cerbere-catch-local-vars)

(defcustom cerbere-catch-custom-get-tag-function nil
  "Alternative function for getting the current tag.

If set to a function the supplied function will be called to get
the tag name.  Use this if you have a special convention to name
your tags."

  :type 'sexp
  :group 'cerbere-catch-local-vars)

(defcustom cerbere-catch-custom-get-test-name-function nil
  "Alternative function for getting the current test name.

If set to a function the supplied will be called to get the name
of the test.  Use this if you have a special convention to name
your tests."

  :type 'sexp
  :group 'cerbere-catch-local-vars)


;; constants

(defconst cerbere-catch-test-regexp
  (concat
   ;; check for any test macro and allow possible leading whitespace
   "^ ?\\(TEST_CASE(\\|TEST_CASE_METHOD(\\|SCENARIO(\\)"
   ;; this matches a classname no quotes allowed here and the whole
   ;; argument is optional
   "\\(\\([^ )\"]+?\\), ?\\)?"
   ;; this matches the test name everything is allowed here but must
   ;; be in double quotes. This one is NOT optional.
   "\\(\"\\([^)]+?\\)\",? ?\\)"
   ;; last argument is the tag and is enclosed in square brackets and
   ;; double quotes. This one is also optional.
   "\\(\"\\(\\[[^ )]+?]\\)\"\\)?)")
  "Regexpression for any of the test macros provided by catch.")

(defconst cerbere-catch-regexp-macro-pos 1
  "Group number of the test macro in `cerbere-catch-test-regxp'.

To be used with `match-string'")

(defconst cerbere-catch-regexp-class-name-pos 3
  "Group number of the class name in `cerbere-catch-test-regxp'.

To be used with `match-string'.")

(defconst cerbere-catch-regexp-test-name-pos 5
  "Group number of the test name in `cerbere-catch-test-regxp'.

To be used with `match-string'.")

(defconst cerbere-catch-regexp-tag-pos 7
  "Group number of tag in `cerbere-catch-test-regxp'.

To be used with `match-string'.")

(defconst cerbere-catch-special-tags
  '("\\[.]" "\\[hide]" "\\[!hide]"
    "\\[!throws]"
    "\\[!shouldfail]"
    "\\[!mayfail]")
  "List of special tags which need to be filtered.

The strings are regexp-quoted as they are used to build up the
final regexp for filtering in
`cerbere-catch--filter-special-tags'.")


;; Internal functions

(defun cerbere-catch--get-root-directory()
  "Return the root directory to run tests."
  (let ((filename (buffer-file-name)))
    (when filename
      (file-truename (or (locate-dominating-file filename ".git")
                         (locate-dominating-file filename "Makefile")
                         "./")))))

(defun cerbere-catch--break-into-debugger-advice (orig-fun &rest args)
  "Experimental advice for cerbere--build for --break option.

Doesn't start the normal compile stuff but starts gdb with given
commands.  ORIG-FUN is discarded and ARGS contains only the command
given from `cerbere-catch--get-program'."
  (advice-remove 'cerbere--build #'cerbere-catch--break-into-debugger-advice)
  (let ((command (car args))
        (root-dir (cerbere-catch--get-root-directory)))
    (with-temp-buffer
      (cd root-dir)
      ;; results in some funny "Bad JSON object" errors and the
      ;; first run of the debugged program will produce output in the
      ;; wrong buffer if started with `gdb-many-windows'
      (gdb (concat  "gdb -i=mi --ex run --args " command)))))

(defun cerbere-catch--get-program (args)
  "Return the command to launch unit test.

`ARGS' corresponds to Catchs command line arguments.  First the
test binary will be compiled and then called from project root."

  (if cerbere-catch-program
      (if cerbere-catch-break-into-debugger
          (progn (advice-add 'cerbere--build
                             :around #'cerbere-catch--break-into-debugger-advice)
                 (concat cerbere-catch-program " " args))

          (concat "cd " (cerbere-catch--get-root-directory) " && "
                  cerbere-catch-compile-command " && "
                  cerbere-catch-program " " args))
    (error "Var cerbere-catch-program not set")))

(defun cerbere-catch--arguments (args)
  "Derive the argument string from `ARGS' and set vars."
  (string-join
   (cl-remove-if-not
    (lambda (entry) entry)
    (list "--use-colour no"
          "--filenames-as-tags"
          "--durations"
          (if cerbere-catch-show-durations
              "yes"
            "no")
          (when cerbere-catch-stop-on-failure
            "--abort")
          (when cerbere-catch-stop-on-x-failures
            (concat "--abortx " cerbere-catch-stop-on-x-failures))
          (when cerbere-catch-no-throw
            "--notrow")
          (when cerbere-catch-include-successful
            "--success")
          ;; would be cool if emacs gdb interface could work with this
          (when cerbere-catch-break-into-debugger
            "--break")
          (when cerbere-catch-order
            (concat "--order " cerbere-catch-order))
          (when cerbere-catch-rng-seed
            (concat " --rng-seed " cerbere-catch-rng-seed))
          args))
   " "))

(defun cerbere-catch--testfile-p ()
  "Check if file is a test file."
  (save-excursion
    (or (search-backward-regexp "#include <catch.h[p+]?[p+]?>" nil t)
        (progn
          (goto-char 0)
          (search-forward-regexp cerbere-catch-test-regexp nil t)))))

(defun cerbere-catch--filter-special-tags (tag-str)
  "Filter all of catchs special tags in TAG-STR.

The special tags are defined in `cerbere-catch-special-tags'."

  (when tag-str
    (replace-regexp-in-string (cl-reduce (lambda (a b)
                                           (concat a "\\|" b))
                                         cerbere-catch-special-tags)
                              ""
                              tag-str)))

(defun cerbere-catch--get-current-tag-and-test-from-testfile ()
  "Try to find current tag and test name in current test file."
  (if (cerbere-catch--testfile-p)
      (save-excursion
        (let ((search  (or (end-of-line)
                           ;; go to end of line and search backwards
                           (search-backward-regexp cerbere-catch-test-regexp nil t)
                           ;; if nothing matched search forward
                           (search-forward-regexp cerbere-catch-test-regexp nil t)))
              (class  (match-string cerbere-catch-regexp-class-name-pos))
              (test   (if (equal "SCENARIO(" (match-string
                                              cerbere-catch-regexp-macro-pos))
                          (concat "\"SCENARIO: "
                                  (match-string cerbere-catch-regexp-test-name-pos)
                                  "\"")
                        (concat "\""
                                (match-string cerbere-catch-regexp-test-name-pos)
                                "\"")))
              (tag    (cerbere-catch--filter-special-tags
                       (match-string cerbere-catch-regexp-tag-pos))))
          (if search
              (list test tag)
            (error "No test found"))))
    (error "Not in test file")))


(defun cerbere-catch--get-current-tag ()
  "Try to get current tag name.

Calls `cerbere-catch-custom-get-tag-function' if set to a
function.  Otherwise gets the buffername without extension and
appends `cerbere-catch-testfile-suffix' and prepends
`cerbere-catch-testfile-prefix' if they are set (only if not in
test file itself! See note below).

NOTE: Can behave in two slightly different ways.  If called from
a test file the tag will be the one specified in the TEST_CASE
macro.  If called from a non test file catchs
'--filenames-as-tag' option will be used."

  (if (cerbere-catch--testfile-p)
      (cadr (cerbere-catch--get-current-tag-and-test-from-testfile))
    (if (functionp cerbere-catch-custom-get-tag-function)
        (funcall cerbere-catch-custom-get-tag-function)
      (concat "[#"
              (or cerbere-catch-testfile-prefix "")
              (file-name-sans-extension (buffer-name))
              (or cerbere-catch-testfile-suffix "")
              "]"))))

(defun cerbere-catch--get-current-test ()
  "Try to get current test and tag name.

Calls `cerbere-catch-custom-get-test-name-function' if set to a
function.  Otherwise calls `which-function' and appends
`cerbere-catch-testcase-suffix' and prepends
`cerbere-catch-testcase-prefix' if they are set.  Prepends the
output of `cerbere-catch--get-current-tag'."

  (if (cerbere-catch--testfile-p)
      (let ((list (cerbere-catch--get-current-tag-and-test-from-testfile)))
        (string-join (list (cadr list) (car list)) " "))
    (if (functionp cerbere-catch-custom-get-test-name-function)
        (funcall cerbere-catch-custom-get-test-name-function)
      (let* ((list  (last (split-string (which-function) ":" t) 2))
             (length (length list))
             (tag (cerbere-catch--get-current-tag))
             (func  (if (> length 1)
                        (substring (cadr list) 0 (string-match "(" (cadr list)))
                      (substring (car list) 0 (string-match "(" (car list))))))
        (concat tag " "
                (or cerbere-catch-testcase-prefix "")
                func
                (or cerbere-catch-testcase-suffix ""))))))

(defun cerbere-catch--run (args)
  "Run cerbere-catch with ARGS."
  (cerbere--build (cerbere-catch--get-program
                   (cerbere-catch--arguments args))))


;; API for cerbere

(defun cerbere-catch--current-test ()
  "Launch Catch on current test."
  (interactive)
  (let ((args (cerbere-catch--get-current-test)))
    (cerbere-catch--run args)))


(defun cerbere-catch--current-file ()
  "Launch Cerbere on current file.

For Catch this means run tests for tag which has the name of the
file."

  (interactive)
  (let ((args (concat (cerbere-catch--get-current-tag))))
    (cerbere-catch--run args)))


(defun cerbere-catch--current-project ()
  "Launch Catch on current project."
  (interactive)
  (cerbere-catch--run ""))

;;;###autoload
(defun cerbere-catch (command)
  "Catch cerbere backend.

COMMAND can either be 'test 'file or 'project."

  (pcase command
    (`test (cerbere-catch--current-test))
    (`file (cerbere-catch--current-file))
    (`project (cerbere-catch--current-project))))

;;;###autoload
(defun cerbere-catch-setup ()
  "Set up cerbere to use Catch for C++ files and."
  (interactive)
  (add-to-list 'cerbere-backends '("c++" . cerbere-catch))
  (add-to-list 'cerbere-backends '("cpp" . cerbere-catch))
  (add-to-list 'cerbere-backends '("h" . cerbere-catch))
  (add-to-list 'cerbere-backends '("h++" . cerbere-catch))
  (add-to-list 'cerbere-backends '("hpp" . cerbere-catch)))

(provide 'cerbere-catch)
;;; cerbere-catch.el ends here
